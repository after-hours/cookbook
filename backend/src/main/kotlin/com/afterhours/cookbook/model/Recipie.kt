package com.afterhours.cookbook.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "recipie")
data class Recipie(
        var id: Long? = null
)